# Android Search View [![](https://jitpack.io/v/com.gitlab.massarttech/android-search-view.svg)](https://jitpack.io/#com.gitlab.massarttech/android-search-view)
**Android Search view like Google Photos Android app.**

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:android-search-view:1.0.0'
}

## SearchView
```java
val searchView = findViewById<SearchView>(R.id.searchView)
```

### XML
```xml
        <com.massarttech.android.search.widget.SearchView
            android:id="@+id/searchView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />
```

### XML attributes
```xml
        <attr name="search_navigation_icon_support" format="enum">
            <enum name="hamburger" value="100" />
            <enum name="arrow" value="101" />
            <enum name="animation" value="102" />
        </attr>
```

## SearchMenuItem
```java
val searchMenuItem = findViewById<SearchMenuItem>(R.id.searchMenuItem)
```

### XML
```xml
        <com.massarttech.android.search.widget.SearchMenuItem
            android:id="@+id/searchMenuItem"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />
```

### XML attributes
```xml
        <attr name="search_navigation_icon_support" format="enum">
            <enum name="hamburger" value="100" />
            <enum name="arrow" value="101" />
            <enum name="animation" value="102" />
        </attr>
```

## Changelog
**1.0.0**
- first release

## Credit

* **Martin Lapiš** - [GitHub](https://github.com/lapism)
